from django.urls import path, re_path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('articles/2003/', views.special_case_2003, name='special'),
    re_path(r'^articles/(?P<year>[0-9]{4})/$', views.year_archive, name='years_archive'),
    path('articles/<int:year>/<int:month>/', views.month_archive, name='month_archive'),
    path('articles/<int:year>/<int:month>/<slug:slug>/', views.article_detail, name='articles_archive'),
]
# str "/" - exclude
# int 0, 1, ... (>=0)
# slug - ASCII char + number + - + _
# uuid - UUID format
# path - include "/"

# reverse
# reverse_lazy


